import React, {useState, useEffect} from "react";
// import Particles from 'react-particles-js';

function Landing(props){

    return(
        <div class="landing-main">
            <div class="landing-navbar">
                <div class="landing-icon">
                    <h1 class="landing-logo"> Stonks </h1>
                </div>
            </div>
        
            <div class="landing-content">
                <h1 class="landing-head"> Stonks - Stock Price, Charts & News  </h1>
                <p class="paragraph"> We are Stonks, we understand your passion for stock market, 
                    and we are here to help you understand the market and make money. We will give you daily NEWS regarding the events that affect the stock market, we will display you the charts and  
                    every resources required to help you make a better investor. 
                </p><br/>
                <br/>
                <p2 class="C3"> Investment simplifyed </p2><br/>
                <br/>
                <p1 class= "C1"> Invest in 5000+ U.S. Stocks </p1><br/>
                <br/>
                <p3 class= "C2"> without comission with Stonks.</p3>
            </div>
            
            <div class= "signup"> 
                <h1 class =" start"> Is it easy to get started? </h1><br/>
                <br/>
                <p class="start1"> 1. Sign Up in minutes </p>
                <br/>
                <p2 class="start2"> 2. Deposite Funds </p2><br/>
                <br/>
                <p3 class="start3"> 3. Choose your faviurate stock and research  </p3><br/>
                <br/>
                <p4 class="start4">4. Get Started</p4> <br/>
                <br/>


                <button class="b1"> Sign Up</button> <br/>
                <br/>
                <p5 class="or">     or      </p5><br/>
                <br/>
                <button class="b2"> Log in</button> <br/>
                <img class ="startimg" src="/frontend/static/images/start.png"/>
            </div>       
        
       
            <div class ="faq">
            
                
                <h3 class="h3"> Stonks. </h3><br/>
                <br/>
                <p1 class="q1ans"> Stonks is a trading platform for US stocks. It was developed by group-1 of ITC303 & ITC309. It is a platform that
                    helps the traders to invest and trade stocks in the US stock market during the trading hours, i.e. 9:00 am to 4:00 pm from Monday to Friday.
                    The developers have been working on futher development of the platform over time. 
                    </p1><br/>
                
                <br/>
                <p3 class= "q2ans">  Stonks takes the responsibility to purchase or sell an investor's stocks safely but stonks takes no responsibility of the profit or loss
                    faced by the investor.</p3>
                    
            </div>

            <div class="img">
                <img class="fimg" src="./images/Stock222.jpg" />
            </div>

            <div class="LGT">
                <p> LETS GROW TOGETHER!!! </p>
            </div>

        </div>

    );
}

export default Landing;