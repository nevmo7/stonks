import React, { useEffect, useState } from "react";
import {Search, Label } from 'semantic-ui-react';
import { Link, Redirect } from 'react-router-dom';

export default function SearchStock(props) {

    const[loading, setLoading] = useState(false);
    const[searchResult, setSearchResult] = useState([]);
    const resultRenderer = ({ name, symbol }) =>
               <div><p>{name}</p><Label content={symbol} /></div>

    async function search(key){
        if(key !== ""){
            const requestOptions = {
                method: "GET",
                headers: { "Content-Type": "application/json"},
            };
        
            console.log(key);
            setLoading(true)
            let result = await fetch("../stock-api/search/" + key, requestOptions);
            let resultJson = await result.json();

            setSearchResult(resultJson);
            setLoading(false)
        }
        

    }
    
    return(
        <Search 
            placeholder="Search symbol"
            loading={loading}
            onResultSelect={(e, data) => {
                const selectedSymbol = data.results[0].symbol

                if(window.location.pathname.includes("stock"))
                    window.location.replace("./" + selectedSymbol)
                else
                   window.location.replace("./stock/" + selectedSymbol)
                }
            }
            onSearchChange={(e) => search(e.target.value)}
            results={searchResult}
            resultRenderer={resultRenderer}
        />
            
    );

}
