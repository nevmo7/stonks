import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import {Container, Grid, Header, Icon, Table} from 'semantic-ui-react';

function Watchlist(props){

    const[watchlist, setWatchlist] = useState([]);

    useEffect(() => {
        const requestOptions = {
            method: "GET",
            headers: { "Content-Type": "application/json"},
        };
        fetch("/api/get_watchlist", requestOptions)
            .then((response) => response.json())
            .then((data) => {
                if(data[0].status !== "error"){
                    setWatchlist(data);
                }
            });
    }, []);

    function removeFromWatchlist(symbol){
        var csrftoken = getCookie('csrftoken');
        const requestOptions = {
            method: "POST",
            headers: { "Content-Type": "application/json", "X-CSRFToken": csrftoken},
            body: JSON.stringify({
                symbol: symbol,
            }),
        };
        fetch("/api/remove_from_watchlist", requestOptions).then((response) => response.json())
            .then((data) => {
                if(data.Success){
                    const newWatchlist = watchlist.filter((stock) => {
                        return stock.symbol !== symbol
                    })
                    setWatchlist(newWatchlist);
                }
            })
    }

    function getCookie(name) {
        console.log("Getting csrf token");
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    function renderWatchlist(){
        console.log(watchlist);
        const stockList = [];
        Array.from(watchlist).forEach((stock) => {
            var negativeChange;
            if(stock.change.charAt(0) === "-"){
                negativeChange = true
            }else{
                negativeChange = false
            }
            stockList.push(
                <Table.Row >
                    <Table.Cell><Link to={"/stock/" + stock.symbol}>{stock.symbol}</Link></Table.Cell>
                    <Table.Cell>{stock.name}</Table.Cell>
                    <Table.Cell textAlign="right"><span style={{color: negativeChange? "red": "green"}}>{stock.change}</span></Table.Cell>
                    <Table.Cell textAlign="right"><span style={{color: negativeChange? "red": "green"}}>{stock.percent_change}%</span></Table.Cell>
                    <Table.Cell textAlign="right">{stock.close}</Table.Cell>
                    <Table.Cell><a href='javascript:void(0)' onClick={() => removeFromWatchlist(stock.symbol)}><Icon name="close"/></a></Table.Cell>
                </Table.Row>)
       });
       
       return stockList;
    }

    return(
        <Container>
            <Grid>
                <Grid.Row centered>
                <Header as='h1' icon>
                    Watchlist
                    <Header.Subheader>
                    Stocks you are interested in
                    </Header.Subheader>
                </Header>
                </Grid.Row>
            </Grid>
            <Table basic='very' selectable>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Ticker</Table.HeaderCell>
                        <Table.HeaderCell>Name</Table.HeaderCell>
                        <Table.HeaderCell textAlign="right">Day change</Table.HeaderCell>
                        <Table.HeaderCell textAlign="right">Day % change</Table.HeaderCell>
                        <Table.HeaderCell textAlign="right">Last price</Table.HeaderCell>
                        <Table.HeaderCell>Remove</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {watchlist.length === 0? <h4>Nothing in watchlist</h4> : renderWatchlist()}
                </Table.Body>

            </Table>
        </Container>
    );
}

export default Watchlist
